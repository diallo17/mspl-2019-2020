# MSPL-2019-2020

Modèles statistiques et Programmation Lettrée Licence MIAGE 2018-2019

ATTENTION CE README CONTIENT DES LIENS QUI SERONT ACTIVÉS AU FUR ET À MESURE

The course is organized by sessions mixing fundamentals and activities

## Fundamentals
0. [General Introduction](./Presentations/0-Introduction/mspl-2019-0-introduction.pdf) - Course overview, goals, references and reproducibility
1. [Literate Programming](./Presentations/1-Litterate-Programming/mspl-2019-1-literate-programming.pdf) - Literate Programming Motivation & RStudio Case Study
2. [Data Representation](./Presentation/2-Visualization/main/Visualisation.2017.02.07.pdf) - Checklist for good graphics ([Slides for Discussion](./Presentation/2-Visualization/2-Visualization.pdf))
3. [Data Manipulation](./Presentation/3-Manipulation/3-Manipulation.pdf) - Using the dplyr R package to manipulate data
4. [Probability / Statistics](./Presentation/4-RevProbability/3_introduction_to_statistics_sel.pdf) - an Introduction to Probability and Statistics (revision)

## Activities
## Specifications for *Travaux dirigés*

1. [Git setup and kickoff](./Activities/TD1.espec.md)
2. [Critical view for plots](./Activities/TD2.espec.md)
3. [Using RStudio for running a Statistical Analysis](./TD3.espec.md)
4. [Combining RStudio and The Grammar of Graphics (of ggplot2)](./TD4.espec.md)
5. [Data Manipulation with dplyr](./TD5.espec.md)
<!-- 6. [Galilée et le Paradoxe du Duc de Toscane](./TD6.espec.md) -->
<!-- 7. [Hypothesis Test : le Paradoxe du Duc de Toscane (la suite)](./TD7/TD7.Rmd) -->

## Additional resources

- [RStudio Cheat Sheets](https://www.rstudio.com/resources/cheatsheets/)
- [Interesting approach to understand ggplot2](https://evamaerey.github.io/ggplot_flipbook/ggplot_flipbook_xaringan.html#1)

## Contact Information

If you encounter any problem, please contact the professors by e-mail:
- Jean-Marc Vincent (jean-marc.vincent@imag.fr)
- Tom Cornebize (tom.cornebize@inria.fr)

## Final Project

Le mini-projet, à faire en binome,  doit être rendu le **... mars avant minuit**,

- [Specification of the **Mini-Projet**](./Project.espec.md)

sur le git dans un dossier spécifique intitulé Projet contenant le fichier R markdown (Rmd), le fichier pdf généré par knitr, les données auxiliaires si besoin)
utiliser L3-MIAGE-MSPL-Nom1-Nom2.* comme nom de fichiers.

Vous devez nous signaler que les fichiers ont été déposés par un mail à l'attention de 

Jean-Marc.Vincent@univ-grenoble.fr et tom.cornebize@inria.fr

entête du message : [L3-MIAGE:MSPL] mini-projet Nom1 Nom2

Corps du message : (en plus du contenu traditionnel)

lien vers le Rmd

lien vers le pdf

adresse d'envoi : etu.univ-grenoble-alpes.fr

Nous envisageons une
présentation "ultra-courte" (5 minutes) pour convaincre le ... avril.
